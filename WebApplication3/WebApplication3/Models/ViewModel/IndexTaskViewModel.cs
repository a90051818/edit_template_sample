﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication3.Models;

namespace WebApplication3.ViewModels.Task
{
    public class IndexTaskViewModel
    {
        public User User { get; set; }
        public List<UserTask> UserTasks { get; set; }
    }
}