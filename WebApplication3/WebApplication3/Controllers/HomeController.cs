﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Edit()
        {
            // prepare default view model for testing
            var viewModel = new ViewModels.Task.IndexTaskViewModel();
            viewModel.User = new Models.User() { Name = "Chirs Chen", Phone = "0800-000-000", UserId = "Chris" };
            viewModel.UserTasks = new List<Models.UserTask>()
    {
        new Models.UserTask() {UserId = "Chris", TaskName="do something 1", CompletedDate = System.DateTime.Now},
        new Models.UserTask() {UserId = "Chris", TaskName="do something 2", CompletedDate = System.DateTime.Now},
        new Models.UserTask() {UserId = "Chris", TaskName="do something 3", CompletedDate = System.DateTime.Now},
        new Models.UserTask() {UserId = "Chris", TaskName="do something 4", CompletedDate = System.DateTime.Now},
    };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(ViewModels.Task.IndexTaskViewModel viewModel)
        {
            // save ...
            return RedirectToAction("Index");
        }
    }
}